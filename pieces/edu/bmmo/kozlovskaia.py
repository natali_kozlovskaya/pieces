import numpy as np
import scipy.stats as stats

# mean 
def mean_count(p, x):
    return sum(x * p)   

# variance
def variance_count(p, x):
    m = mean_count(p, x)
    m_square = mean_count(p, x * x)
    return m_square - (m ** 2)


def pa(params, model):
    a_arr = np.arange(params['amin'], params['amax'] + 1)
    n = params['amax'] - params['amin'] + 1
    p = np.array([1. / n] * n)
    return p, a_arr


def pb(params, model):
    b_arr = np.arange(params['bmin'], params['bmax'] + 1)
    n = params['bmax'] - params['bmin'] + 1
    p = np.array([1. / n] * n)
    return p, b_arr


def pc(params, model):
    a_arr = np.arange(params ['amin'], params ['amax'] + 1)
    b_arr = np.arange(params ['bmin'], params ['bmax'] + 1)
    if model == 2:
        x = stats.poisson.pmf(np.arange(params['amax'] + 1), a_arr[:, np.newaxis] * params['p1'])
        y = stats.poisson.pmf(np.arange(params['bmax'] + 1), b_arr[:, np.newaxis] * params['p2'])
    else:
        x = stats.binom.pmf(np.arange(params['amax'] + 1), a_arr[:, np.newaxis ], params ['p1'])
        y = stats.binom.pmf(np.arange(params['bmax'] + 1), b_arr[:, np.newaxis ], params ['p2'])
    x_ = np.sum(x, axis = 0)
    y_ = np.sum(y, axis = 0)
    p = np.convolve(x_, y_)
    p /= np.sum(p) 
    return p, np.arange(params['amax'] + params['bmax'] + 1)


def pd_c(c, params, model):
    d_arr = np.arange(2 * (params['amax'] + params['bmax']) + 1)
    p = np.zeros(len(d_arr))
    p[c : (2 * c + 1)] = stats.binom.pmf(np.arange(c+1), c, params['p3'])
    return p, d_arr


def pd(params, model):
    d_arr = np.arange(2 * (params['amax'] + params['bmax']) + 1)
    c_arr = np.arange(params['amax'] + params['bmax'] + 1)
    pd_c_matrix = np.empty((len(d_arr), len(c_arr)))
    for c_value in c_arr:
        pd_c_matrix[:, c_value] = pd_c(c_value, params, model)[0]
    return np.dot(pd_c_matrix, pc(params, model)[0]), d_arr


def conv_matrix(vec, dim):
    res = np.zeros(shape=(len(vec) + dim-1, dim))
    for i in range(dim):
        res[i:i+len(vec), i] = vec[:len(vec)]
    return res


def pb_ad(a_param, d_param, params, model):
    
    b = np.arange(params['bmin'], params['bmax'] + 1)
    c = np.arange(params['amax'] + params['bmax'] + 1)
    
    c_len = params['amax'] + params['bmax'] + 1
    d_len = 2 * (params['amax'] + params['bmax'])  + 1
    
    # pd_c
    pd_c_matrix = np.empty((d_len, c_len))
    for c_value in range(c_len):
        pd_c_matrix[:, c_value] = pd_c(c_value, params, model)[0]
    pd_c_vec = pd_c_matrix[d_param, :]
    
    # model 1
    if model == 1:
        x = stats.binom.pmf(np.arange(a_param + 1), a_param, params['p1'])
        conv = conv_matrix(x, params['bmax']+1) 
        y = stats.binom.pmf(np.arange(params['bmax'] + 1)[:, np.newaxis], b, params['p2'])
        
    # model 2
    else:
        x = stats.poisson.pmf(np.arange(a_param + 1), a_param * params['p1'])
        conv = conv_matrix(x, params['bmax']+1)
        y = stats.poisson.pmf(np.arange(params['bmax'] + 1)[:, np.newaxis], b * params['p2'])
    res_ = np.dot(conv, y)
    res = np.zeros(shape=(c_len, len(b)))
    res[:res_.shape[0], :res_.shape[1]] = res_
    prob = np.dot(pd_c_vec.T, res)
        
    return prob / np.sum(prob), b


def pc_ab(a_value, b_value, params, model):
    p = np.zeros(params['amax'] + params['bmax'] + 1)
    c = np.arange(params['amax'] + params['bmax'] + 1)
    if model==2:
        x = stats.poisson.pmf(np.arange(params['amax'] + 1), a_value * params['p1'])
        y = stats.poisson.pmf(np.arange(params['bmax'] + 1), b_value * params['p2'])
        p = np.convolve(x, y)
        p /= np.sum(p)
    else:
        x = stats.binom.pmf(np.arange(params['amax'] + 1), a_value, params['p1'])
        y = stats.binom.pmf(np.arange(params['bmax'] + 1), b_value, params['p2'])
        p = np.convolve(x, y)
        p /= np.sum(p)
    return p, c


'''def pb_d(d_param, params, model):
    
    a_array = np.arange(params['amin'], params['amax'] + 1)
    b_array = np.arange(params['bmin'], params['bmax'] + 1)
    c_array = np.arange(params['amax'] + params['bmax'] + 1)
    c_len = params['amax'] + params['bmax'] + 1
    a_len = params['amax'] - params['amin']  + 1
    b_len = params['bmax'] - params['bmin']  + 1
    d_len = 2 * (params['amax'] + params['bmax'])  + 1
    
    p_d_c_matrix = np.empty((d_len, c_len))
    for c_value in c_array:
        p_d_c_matrix[:, c_value] = pd_c(c_value, params, model)[0]
    
    
    p_cab = np.zeros(shape = (c_len, a_len, b_len))
    for a_i in range(a_len):
        #p_cab[:, a_i, :] = pc_a(a_array[a_i], params, model)[0]
        if model == 1:
            x = stats.binom.pmf(np.arange(a_i + 1), a_i, params['p1'])
            conv = conv_matrix(x, params['bmax']+1)
            y = stats.binom.pmf(np.arange(params['bmax'] + 1)[:, np.newaxis], b, params['p2'])
            
        else:
            x = stats.poisson.pmf(np.arange(a_i + 1), a_i * params['p1'])
            conv = conv_matrix(x, params['bmax']+1) #x, 601
            y = stats.poisson.pmf(np.arange(params['bmax'] + 1)[:, np.newaxis], b * params['p2'])
        res_ = np.dot(conv, y)
        res = np.zeros(shape=(c_len, len(b)))
        res[:res_.shape[0], :res_.shape[1]] = res_
        p_cab[:, a_i, :] = res
        #for b_i in range(b_len):
            #p_cab[:, a_i, b_i] = pc_ab(a_array[a_i], b_array[b_i], params, model)[0]

    tensor = np.einsum('ij,jkl->ikl', p_d_c_matrix, p_cab)
    pd_b = np.sum(tensor, axis=1)

    # + norm const
    pd_b *= pa(params, model)[0][0] * pb(params, model)[0][0]

    return pd_b[d_param] / np.sum(pd_b[d_param]), b_array'''

def pb_d(d_param, params, model):
    
    a_array = np.arange(params['amin'], params['amax'] + 1)
    b_array = np.arange(params['bmin'], params['bmax'] + 1)
    c_array = np.arange(params['amax'] + params['bmax'] + 1)
    c_len = params['amax'] + params['bmax'] + 1
    a_len = params['amax'] - params['amin']  + 1
    b_len = params['bmax'] - params['bmin']  + 1
    d_len = 2 * (params['amax'] + params['bmax'])  + 1
    
    p_d_c_matrix = np.empty((d_len, c_len))
    for c_value in c_array:
        p_d_c_matrix[:, c_value] = pd_c(c_value, params, model)[0]
    
    
    p_cab = np.zeros(shape = (c_len, a_len, b_len))
    for a_i in range(a_len):
        for b_i in range(b_len):
            p_cab[:, a_i, b_i] = pc_ab(a_array[a_i], b_array[b_i], params, model)[0]

    tensor = np.einsum('ij,jkl->ikl', p_d_c_matrix, p_cab)
    pd_b = np.sum(tensor, axis=1)

    pd_b *= pa(params, model)[0][0] * pb(params, model)[0][0]

    return pd_b[d_param] / np.sum(pd_b[d_param]), b_array



def pc_a(a_param, params, model):
    c_array = np.arange(params['amax'] + params['bmax'] + 1)
    b = np.arange(params['bmin'], params['bmax'] + 1)
    
    c_len = params['amax'] + params['bmax'] + 1
    a_len = params['amax'] - params['amin']  + 1
    b_len = params['bmax'] - params['bmin']  + 1
    
    
    # model 1
    if model == 1:
        x = stats.binom.pmf(np.arange(a_param + 1), a_param, params['p1'])
        conv = conv_matrix(x, params['bmax']+1)
        y = stats.binom.pmf(np.arange(params['bmax'] + 1)[:, np.newaxis], b, params['p2'])
        
    # model 2
    else:
        x = stats.poisson.pmf(np.arange(a_param + 1), a_param * params['p1'])
        conv = conv_matrix(x, params['bmax']+1) 
        y = stats.poisson.pmf(np.arange(params['bmax'] + 1)[:, np.newaxis], b * params['p2'])
    res_ = np.dot(conv, y)
    res = np.zeros(shape=(c_len, len(b)))
    res[:res_.shape[0], :res_.shape[1]] = res_
    prob = res.sum(axis=1)
    return prob / np.sum(prob), c_array


def pc_b(b_value, params, model):
    c_array = np.arange(params['amax'] + params['bmax'] + 1)
    
    c_len = params['amax'] + params['bmax'] + 1
    a_len = params['amax'] - params['amin']  + 1
    b_len = params['bmax'] - params['bmin']  + 1
    p_ca_b_matrix = np.zeros(shape=(c_len, a_len))
    
    for a_value in range(a_len):
        p_ca_b_matrix[:, a_value] = pc_ab(a_value, b_value, params, model)[0]
        
    p_a = pa(params, model)[0]
    return np.dot(p_ca_b_matrix, p_a), c_array

    