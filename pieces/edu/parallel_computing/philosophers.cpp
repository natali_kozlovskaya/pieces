#include <atomic>
#include <cstdlib>
#include <cstdio>
#include <deque>
#include <random>
#include <thread>
#include <vector>
#include <mutex>
#include <condition_variable>
#include <iostream>

unsigned debug_flag;

void sleep(unsigned milliseconds) {
    std::this_thread::sleep_for(std::chrono::milliseconds(milliseconds));
}

class Fork {
public:
    Fork() : fork_mutex() {

    }

    void take() {
        fork_mutex.lock();
    }

    void put() {
        fork_mutex.unlock();
    }
    std::mutex fork_mutex;

private:

};

typedef std::chrono::microseconds Microseconds;
typedef std::chrono::steady_clock Clock;
typedef Clock::time_point Time;


class Waiter;

class Philosopher {
public:
    // friend Waiter;
    Philosopher(
            unsigned id,
            Fork* fork_left, Fork* fork_right,
            unsigned think_delay, unsigned eat_delay) :
            id(id),
            fork_left(fork_left), fork_right(fork_right),
            r_engine(std::random_device()()),
            think_delay_dist(0, think_delay),
            eat_delay_dist(0, eat_delay),
            eat_count(0), wait_time(0), stop_flag(false),
            permition_flag(false), eating_flag(false),
            hungry_flag(false){
    }

    int GetEatCount() {
        return eat_count;
    }

    bool GetEatFlag() {
        return eating_flag;
    }

    bool GetHungryFlag() {
        return hungry_flag;
    }

    bool GetPermitionFlag() {
        return permition_flag;
    }

    void SetPermitionFlag() {
        permition_flag = true;
    }

    void run() {
        while (!stop_flag) {

            if (!hungry_flag) {
                think();
                hungry_flag = true;
            }
            std::unique_lock<std::mutex> mutex_for_waiter(default_mutex);
            hungry_flag = true;
            while (! permition_flag) {
                wake_philosopher.wait(mutex_for_waiter);
            }


            if (permition_flag) {
                hungry_flag = false;
                eating_flag = true;
                eat();
                eating_flag = false;
                permition_flag = false;
            }
        }
        if (debug_flag) std::printf("[%u] stopped\n", id);
    }

    void stop() {
        stop_flag = true;
    }

    void printStats() const {
        std::printf("[%u] %u %lld\n", id, eat_count, wait_time);
    }

    std::mutex default_mutex;
    std::condition_variable wake_philosopher;

private:
    void think() {
        if (debug_flag) std::printf("[%u] thinking\n", id);
        sleep(think_delay_dist(r_engine));
        if (debug_flag) std::printf("[%u] hungry\n", id);
        wait_start = Clock::now();
    }

    void eat() {
        fork_left->take();
        if (debug_flag) std::printf("[%u] took left fork\n", id);
        fork_right->take();
        if (debug_flag) std::printf("[%u] took right fork\n", id);

        ///
        wait_time += std::chrono::duration_cast<Microseconds>(
                Clock::now() - wait_start).count();
        if (debug_flag) std::printf("[%u] eating\n", id);
        sleep(eat_delay_dist(r_engine));
        ++eat_count;
        ///

        fork_right->put();
        if (debug_flag) std::printf("[%u] put right fork\n", id);
        fork_left->put();
        if (debug_flag) std::printf("[%u] put left fork\n", id);
    }


    unsigned id;
    Fork* fork_left;
    Fork* fork_right;
    std::default_random_engine r_engine;
    std::uniform_int_distribution<unsigned> think_delay_dist;
    std::uniform_int_distribution<unsigned> eat_delay_dist;
    unsigned eat_count;
    long long wait_time;
    Time wait_start;
    std::atomic<bool> stop_flag;
    //

    std::atomic<bool> eating_flag;
    std::atomic<bool> hungry_flag;
    std::atomic<bool> permition_flag;
};

class Waiter {
public:
    Waiter(std::deque<Philosopher>* phils):phils(phils), stop_flag(false){
    }

    void run() {

        while (!stop_flag) {
            for (int index = 0; index < phils->size(); ++index) {
                // if index is hungry
                if (stop_flag) {
                    break;
                }

                if (phils->at(index).GetHungryFlag()) {
                    // if neghbours are not eating
                    if (!phils->at((index - 1 + phils->size()) % phils->size()).GetPermitionFlag()
                        && !phils->at((index + 1 + phils->size()) % phils->size()).GetPermitionFlag()) {
                        phils->at(index).SetPermitionFlag();
                        phils->at(index).wake_philosopher.notify_one();
                        ++index;
                    }
                }
            }


        }
    }

    void stop() {
        stop_flag = true;
    }


private:
    std::deque<Philosopher>* phils;
    std::atomic<bool> stop_flag;
};



int main(int argc, char* argv[]) {
    if (argc != 6) {
        std::fprintf(stderr,
                     "Usage: %s phil_count duration think_delay eat_delay debug_flag\n",
                     argv[0]);
        return 1;
    }

    unsigned N = std::atoi(argv[1]); //5
    unsigned duration = std::atoi(argv[2]); //20
    unsigned think_delay = std::atoi(argv[3]); //30
    unsigned eat_delay = std::atoi(argv[4]); //10
    debug_flag = std::atoi(argv[5]); //1

    std::setvbuf(stdout, NULL, _IONBF, BUFSIZ);

    std::vector<Fork> forks(N);

    std::deque<Philosopher> phils;
    for (unsigned i = 0; i < N; ++i) {
        phils.emplace_back(i + 1,
                           &forks[(i + 1) % N], &forks[i],
                           think_delay, eat_delay);
    }


    //  waiter
    Waiter waiter(&phils);
    std::thread waiter_thread;
    std::vector<std::thread> threads;
    threads.reserve(N);
    for (auto& phil : phils) {
        threads.emplace_back([&phil] { phil.run(); });
    }
    waiter_thread = std::thread(&Waiter::run, &waiter);

    sleep(duration * 1000);
    for (auto& phil : phils) {
        phil.stop();
    }


    for (auto& thread : threads) {
        thread.join();
    }
    waiter.stop();
    waiter_thread.join();

    for (const auto& phil : phils) {
        phil.printStats();
    }
}