#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>

using std::vector;
using std::int64_t;

// struct for one player, his strength and number
struct Player {
  int player_strength;
  int player_number;
};

template<typename Iterator, typename OutputIterator, typename Comparator>
OutputIterator Merge(Iterator begin_first, Iterator end_first,
                     Iterator begin_second, Iterator end_second,
                     OutputIterator output_iterator, Comparator comparator) {

  while (begin_first != end_first && begin_second != end_second) {
    *output_iterator = (comparator(*begin_first, *begin_second)) ?
                      *begin_first++ : *begin_second++;
    ++output_iterator;
  }

  if (begin_first != end_first) {
    std::copy(begin_first, end_first, output_iterator);
  }

  if (begin_second != end_second) {
    std::copy(begin_second, end_second, output_iterator);
  }
  return output_iterator;
}

template<typename Iterator, typename Comparator>
void Sort(Iterator begin, Iterator end, Comparator comparator) {
  int size = std::distance(begin, end);
  if (size < 2) {
    return;
  }
  auto middle = begin + (size / 2);
  Sort(begin, middle, comparator);
  Sort(middle, end, comparator);

  vector<typename Iterator::value_type> merged(size);
  Iterator iterator = Merge(begin, middle, middle, end, merged.begin(), 
                            comparator);
  std::move(merged.begin(), merged.begin() + size, begin);
}

// comparator from comparing two players by their strength
struct CompareByStrength {
  bool operator() (const Player& one, const Player& other) const {
    if (one.player_strength == other.player_strength) {
      return one.player_number < other.player_number;
    }
    return one.player_strength < other.player_strength;
  }
};

// comparator from comparing two players by their number
struct CompareByNumber {
  bool operator() (const Player& one, const Player& other) const {
    return one.player_number < other.player_number;
  }
};

// function for reading massive
vector<Player> ReadPlayers(std::istream &in_stream = std::cin) {
  int size;
  in_stream >> size;
  vector<Player> all_players;
  for (int i = 0; i < size; ++i) {
    int element;
    in_stream >> element;
    all_players.emplace_back(Player{element, i + 1});
  }
  return all_players;
}

// function for printing result
void PrintResult(int64_t team_strength, vector<Player> team_players,
                 std::ostream &out_stream = std::cout) {
  out_stream << team_strength << std::endl;

  Sort(team_players.begin(), team_players.end(), CompareByNumber());
  for (int i = 0; i < team_players.size(); ++i) {
    out_stream << team_players[i].player_number << " ";
  }
  out_stream << std::endl;
}

// struct for watching current team by method of two pointers
struct TeamInfo {
  int left_player;
  int right_player;
  int64_t efficiency;

  TeamInfo(int left_player_, int right_player_, int64_t efficiency_) {
    left_player = left_player_;
    right_player = right_player_;
    efficiency = efficiency_;
  }
};

// main function that finds strongest solidary team
vector<Player> FindStrongestSolidaryTeam (vector<Player> players) {

  Sort(players.begin(), players.end(), CompareByStrength());

  if (players.size() < 3) {
    return players;
  }

  int64_t strongest_team_efficiency =
      static_cast<int64_t>(players[players.size() - 2].player_strength) +
          players[players.size() - 1].player_strength;
  TeamInfo strongest_team(players.size() - 2, players.size() - 1, 
                          strongest_team_efficiency);
  TeamInfo current_team = strongest_team;

  do {
    while (current_team.left_player > 0 &&
          static_cast<int64_t>(players[current_team.left_player].player_strength) +
            players[current_team.left_player - 1].player_strength >=
            players[current_team.right_player].player_strength) {

      current_team.efficiency += players[current_team.left_player - 1].player_strength;
      current_team.left_player = current_team.left_player - 1;
    }

    if (current_team.efficiency > strongest_team.efficiency) {
      strongest_team = current_team;
    }

    current_team.efficiency -= players[current_team.right_player].player_strength;
    current_team.right_player = current_team.right_player - 1;
  } while (current_team.left_player > 0);

  vector<Player> final_team(std::next(players.begin(), strongest_team.left_player),
                            std::next(players.begin(), strongest_team.right_player + 1));
  return final_team;

}

// function for counting of team's strength
int64_t CountTeamStrength(const vector<Player>& team_players) {
  int64_t team_strength = 0;
  for (int i = 0; i < team_players.size(); ++i) {
    team_strength += team_players[i].player_strength;
  }
  return team_strength;
}

int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);

  vector<Player> players = ReadPlayers();
  vector<Player> team_players = FindStrongestSolidaryTeam(players);
  int64_t team_strength = CountTeamStrength(team_players);
  PrintResult(team_strength, team_players);

  return 0;
}
