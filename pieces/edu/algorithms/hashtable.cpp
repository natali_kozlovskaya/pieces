#include <iostream>
#include <vector>
#include <string>
#include <cstdint>
#include <random>
#include <chrono>
#include <ctime>

using std::vector;
using std::string;
using std::int64_t;

using DoubleVector = vector<vector<int>>;

// this number should be more than range of FixedSet's input-values
const int64_t BIG_PRIME_NUMBER = 4294967311;


// class for hash function
class HashFunction {
 public:
  HashFunction (): slope_(1), constant_term_(0) {}

  HashFunction (int64_t slope, int64_t constant_term):
      slope_(slope), constant_term_(constant_term) {}

  int64_t operator () (int64_t x_variable) const {
    return x_variable > 0 ? (slope_ * x_variable + constant_term_) % BIG_PRIME_NUMBER :
           ((slope_ * x_variable + constant_term_) % BIG_PRIME_NUMBER + BIG_PRIME_NUMBER) %
               BIG_PRIME_NUMBER;
  }

 private:
  int64_t slope_;
  int64_t constant_term_;
};

HashFunction CreateRandomHashFunction(std::mt19937_64 & generator) {
  std::uniform_int_distribution<int64_t> slope_distribution(1, BIG_PRIME_NUMBER);
  std::uniform_int_distribution<int64_t> const_term_distribution(0, BIG_PRIME_NUMBER);

  return HashFunction(slope_distribution(generator), const_term_distribution(generator));
}

// it's a class for hashtable of the second level
// it contains field 'number_' (= number, that second hash table contains.
// and it contains field 'is_empty_'
// second hashtable's dim =  n^2. it means that there are empty cells in it.
// so this field sets, weather it's empty or not.
// So we have massive of instances of this structure instead of just numbers_
// (it's only for hash table of second level)
class Optional {
 public:
  Optional(): number_(0), is_empty_(true) {}
  Optional(int number, bool is_empty):
      number_(number), is_empty_(is_empty) {}

  int number() const {
    return number_;
  }

  bool is_empty() const {
    return is_empty_;
  }

  void set_number(const int number) {
    number_ = number;
    is_empty_ = true;
  }

  void set_is_empty(const bool is_empty) {
    is_empty_ = is_empty;
    number_ = 0;
  }

 private:
  int number_;
  bool is_empty_;
};


// class for hash-tables of second level
class SecondHashTable {
 public:
  SecondHashTable() {}

  void Initialize(const vector<int> &storing_numbers, std::mt19937_64 & generator) {
    // 1. enlarge sizes of hash tables of second level
    numbers_.resize(storing_numbers.size() * storing_numbers.size());
    // 2. fill each hash-table of second level
    FillSecondLevelHashTable(storing_numbers, generator);
  }

  bool Contains(int number) const {
    if (numbers_.empty()) {
      return false;
    }
    // cell in table of second level
    int64_t second_hash = hashFunction_(number) % numbers_.size();
    // result: check, that cell is not empty and that it contains our number
    return (!numbers_[second_hash].is_empty()) && (numbers_[second_hash].number() == number);
  }

 private:
  // hash for table of second level
  HashFunction hashFunction_;
  // after resize vector to dim N^2, some cells in hashtable are empty.
  // see explanation at the beginning (class Optional)
  vector<Optional> numbers_;

  // fill second level hash-table
  void FillSecondLevelHashTable(const vector<int> &storing_numbers,
                                std::mt19937_64 & generator) {

    vector<int>::const_iterator iterator = storing_numbers.begin();

    do {
      hashFunction_ = CreateRandomHashFunction(generator);

      // for each number that should be kept in this hash-table
      // we will define cell
      for (iterator = storing_numbers.begin(); iterator != storing_numbers.end(); ++iterator) {
        int64_t hash = hashFunction_(*iterator) % numbers_.size();

        if (numbers_[hash].is_empty()) {
          // add number
          numbers_[hash] = Optional(*iterator, false);
        } else {
          // collision case: should clear all cells in this table
          numbers_.assign(numbers_.size(), Optional());
          break;
        }
      }
    } while (iterator != storing_numbers.end());
  }
};


class FixedSet {
 public:
  // constructor: it generates hash-coefficients for table of the first level,
  // determines generator
  // creates vector of hash-tables of second level
  FixedSet() : hashFunction_(), secondHashTables_(), generator_(time(NULL)) {}

  void Initialize(const vector<int> &numbers) {

    secondHashTables_.resize(numbers.size());

    // define table of first level with the count of hash tables of second level
    // in it. recieves vector of vectors: which numbers_ from set
    // should be kept in which table of second level
    DoubleVector storing_numbers = BuildFirstHashTable(numbers);

    // building of second hashtable
    for (int i = 0; i < secondHashTables_.size(); ++i) {
      secondHashTables_[i].Initialize(storing_numbers[i], generator_);
    }
  }

  bool Contains(int number) const {
    // in first hashtable (choose hashtable of second level)
    int64_t first_hash = hashFunction_(number) % secondHashTables_.size();
    return secondHashTables_[first_hash].Contains(number);
  }

 private:
  // generator of random numbers_
  std::mt19937_64 generator_;

  static const int memory_limit_multiplier_ = 3;
  // hash for table of first level
  HashFunction hashFunction_;
  // vector of hashtables of second level
  vector<SecondHashTable> secondHashTables_;

  // function defines table of first level with the count of hash tables of second level
  // in it. and we define, which numbers from set should be kept in which table
  // of second level
  DoubleVector BuildFirstHashTable(const vector<int> &numbers) {
    int64_t memory_boundary = 0;
    DoubleVector storing_numbers(secondHashTables_.size());

    do {
      memory_boundary = 0;
      hashFunction_ = CreateRandomHashFunction(generator_);

      // clear second hashtables (in case collision on previous iteration)
      storing_numbers.assign(secondHashTables_.size(), std::vector<int>());

      // fill "storing_numbers" for each hash table of second level
      // we want to receive for each hash table, which number will be kept in it
      for (size_t j = 0; j < numbers.size(); ++j) {
        storing_numbers[hashFunction_(numbers[j]) % secondHashTables_.size()].push_back(numbers[j]);
      }

      // check memory in while-loop (sum of squares - for algorithm it should be
      // < multiplier * numbers_.size())
      for (size_t k = 0; k < secondHashTables_.size(); ++k) {
        memory_boundary += (storing_numbers[k].size() * storing_numbers[k].size());
      }
    } while (memory_boundary > memory_limit_multiplier_ * numbers.size());

    return storing_numbers;
  }
};

// reads input
vector<int> ReadNumbers(std::istream &in_stream = std::cin) {
  int size_set;
  in_stream >> size_set;
  vector<int> numbers;

  for (int j = 0; j < size_set; ++j) {
    int number;
    in_stream >> number;
    numbers.push_back(number);
  }
  return numbers;
}

// prints result
void PrintResult(const vector<bool> &answers, std::ostream &out_stream = std::cout) {
  for (int i = 0; i < answers.size(); ++i) {
    if (answers[i]) {
      out_stream << "Yes" << '\n';
    } else {
      out_stream << "No" << '\n';
    }
  }
}

vector<bool> QueriesProcessing(const vector<int> &set, const vector<int> &queries) {

  // initializing
  FixedSet fixedSet;
  fixedSet.Initialize(set);

  // checking that set is containing
  vector<bool> answers(queries.size());
  for (int i = 0; i < queries.size(); ++i) {
    answers[i] = (fixedSet.Contains(queries[i])) ? true : false;
  }
  return answers;
}


int main() {

  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);
  vector<int> set = ReadNumbers();
  vector<int> queries = ReadNumbers();
  vector<bool> answers = QueriesProcessing(set, queries);
  PrintResult(answers);

  return 0;
}
