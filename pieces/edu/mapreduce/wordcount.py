#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
#
import sys
import re
from itertools import groupby
from operator import itemgetter

class Mapper:

    re_en = re.compile(ur"[a-z]+")
    re_ru = re.compile(ur"[а-яё]+")

    def __init__(self, lang):
        if lang == "en":
            self.re = Mapper.re_en
        else:
            self.re = Mapper.re_ru
        self.results = {}
 

    def run(self):
        doc_count = 0
        data = self.readInput()
        for docid, contents in data:
            text = contents.lower()
            cleaned = ''.join(c for c in text if c.isalpha() or c==' ')
            words = cleaned.split()
            for word in words:
                print ('%s\t%d' % (word, 1)).encode('utf-8') 

    def readInput(self):
        for line in sys.stdin:
            yield unicode(line, 'utf8').strip().split('\t', 1)



class Reducer:

    def run(self):
        data = self.readInput()
        for word, group in groupby(data, itemgetter(0)):
            total_count = sum(int(count) for word, count in group)
            print ('%s\t%d' % (word, total_count)).encode('utf-8')

    def readInput(self):
        for line in sys.stdin:
            yield unicode(line, 'utf8').strip().split('\t', 1)


if __name__ == "__main__":
    mr_func = sys.argv[1]
    if mr_func == "map":
        lang = sys.argv[2]
        mapper = Mapper(lang)
        mapper.run()
    elif mr_func == "reduce":
        reducer = Reducer()
        reducer.run()