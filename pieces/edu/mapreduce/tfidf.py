#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
#

import sys
import re
from itertools import groupby
from operator import itemgetter
from collections import defaultdict
import numpy as np
from collections import Counter

class Mapper:

    re_en = re.compile(ur"[a-z]+")
    re_ru = re.compile(ur"[а-яё]+")

    exclude_ru = [u"в", u"и", u"на", u"с", u"также", u"по",
                     u"года", u"из", u"году", u"а", u"к",
                     u"не", u"от", u"для", u"был", u"что",u"за", u"его", u"как", u"до"]

    exclude_en = ["the", "of" ,"and","in","a","to",
                    "was","is","for","as","on","by",
                    "with","he","at","that","from","his","it","an"]


    def __init__(self, lang):
        if lang == "en":
            self.re = Mapper.re_en
            self.ex = Mapper.exclude_en
        else:
            self.re = Mapper.re_ru
            self.ex = Mapper.exclude_ru
 

    def run(self):
        doc_count = 0
        data = self.readInput()

        dict_of_words = Counter()

        for docid, contents in data:
            text = contents.lower()
            cleaned = ''.join(c for c in text if c.isalpha() or c==' ')
            words = cleaned.split()
            wordcount_in_this_doc = 0

            dict_of_words = Counter()

            for word in words:
                # drop exclude words
                if word in self.ex:
                    continue

                if word not in dict_of_words:
                    dict_of_words[word] = 1
                else:
                    dict_of_words[word] += 1

                wordcount_in_this_doc += 1

            for word in dict(dict_of_words):
                tf = 1. * dict_of_words[word] / wordcount_in_this_doc
                print ('%s+%lf+%s' % (word, tf, docid)).encode('utf-8')
            dict_of_words = Counter()


    def readInput(self):
        for line in sys.stdin:
            yield unicode(line, 'utf-8').strip().split('\t', 1)




class Reducer:

    articles_count_ru = 1236325.
    articles_count_en = 4268154.

    def __init__(self, lang, top_size):

        self.top_size = top_size
        if lang == "en":
            self.art_count = Reducer.articles_count_en
        else:
            self.art_count = Reducer.articles_count_ru

    def run(self):
        data = self.readInput()
        '''for word, tf, docid in data:
            print ('%s+%lf+%s' % (word, float(tf), int(docid))).encode('utf-8')'''

        this_word_in_doc_count = 0
        previous_word = ''

        top_for_word = list()

        for word, tf, docid in data:

            # in case the same word
            if word == previous_word:
                if this_word_in_doc_count < self.top_size:
                    top_for_word.append((float(tf), int(docid)))
                    this_word_in_doc_count+=1

                # cross further to claculate frequency in all docs
                else:
                    this_word_in_doc_count+=1

            # if already another word(key)
            else:
                # print previous word(can calculate idf)
                # make everything zero(counter and so on)
                if previous_word != '':
                    output = previous_word
                    idf = np.log(1. * self.art_count / this_word_in_doc_count)
                    for top in top_for_word:
                        val_1 = str(top[1])
                        val_2 = str(top[0] * idf)
                        output += '\t' + val_1 + ':' + val_2
                    print output.encode('utf-8') 


                this_word_in_doc_count = 0

                previous_word = word

                top_for_word = list()
                top_for_word.append((float(tf), int(docid)))
                this_word_in_doc_count+=1
        # add last word
        output = previous_word
        idf = np.log(1. * self.art_count / this_word_in_doc_count)
        for top in top_for_word:
            val_1 = str(top[1])
            val_2 = str(top[0] * idf)
            output += '\t' + val_1 + ':' + val_2
        print output.encode('utf-8')


    def readInput(self):
        for line in sys.stdin:
            yield unicode(line, 'utf-8').split('+')


if __name__ == "__main__":
    mr_func = sys.argv[1]
    if mr_func == "map":
        lang = sys.argv[2]
        mapper = Mapper(lang)
        mapper.run()
    elif mr_func == "reduce":
        lang = sys.argv[2]
        top_size = int(sys.argv[3]) # 20
        reducer = Reducer(lang, top_size)
        reducer.run()