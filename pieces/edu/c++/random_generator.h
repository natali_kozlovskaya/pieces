#include <iostream>
#include <map>
#include <random>
#include <memory>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <string>
#include <sstream>
#include <chrono>
#include <cstdint>
// #include "rndgen.h"

// #define M_PI 3.14159265358979323846

std::mt19937 generator;

class TRandomNumberGenerator {
 public:
  virtual ~TRandomNumberGenerator() {}
  virtual double Generate() const = 0;
};

class TRandomCreator {
 public:
  virtual TRandomNumberGenerator *CreateObject(std::map<std::string, std::string> &params) const = 0;
};

class TNormalGenerator : public TRandomNumberGenerator {
 private:
  double mean_;
  double variance_;
  // std::mt19937 generator_;

 public:
  TNormalGenerator(double mean, double variance)
      : mean_(mean), variance_(variance) {} // distribution(generator_)

  ~TNormalGenerator() {}

  virtual double Generate() const {
    std::normal_distribution<double> distribution(mean_, variance_);
    return distribution(generator);
  }
};

class TBernoulliGenerator : public TRandomNumberGenerator {
 private:
  double probability_;

 public:
  TBernoulliGenerator(double probability)
      : probability_(probability) {
  }

  ~TBernoulliGenerator() {}

  virtual double Generate() const {
    std::bernoulli_distribution distribution(probability_);
    return distribution(generator);
  }
};

class TLogisticGenerator : public TRandomNumberGenerator {
 private:
  double location_;
  double scale_;

 public:
  TLogisticGenerator(double location, double scale)
      : location_(location), scale_(scale) {
  }

  ~TLogisticGenerator() {}

  virtual double Generate() const {
    std::uniform_real_distribution<double> distribution(0, 1);
    return location_ - scale_ * std::log(1 / distribution(generator) - 1);
  }
};

class TNormalCreator : public TRandomCreator {
 public:
  virtual TRandomNumberGenerator *CreateObject(std::map<std::string, std::string> &params) const {
    double mean = std::stod(params.at("m"));
    double variance = std::stod(params.at("s"));
    return new TNormalGenerator(mean, variance);
  }
};

class TBernoulliCreator : public TRandomCreator {
 public:
  virtual TRandomNumberGenerator *CreateObject(std::map<std::string, std::string> &params) const {
    double probability = std::stod(params.at("p"));
    return new TBernoulliGenerator(probability);
  }
};

class TLogisticCreator : public TRandomCreator {
 public:
  virtual TRandomNumberGenerator *CreateObject(std::map<std::string, std::string> &params) const {
    double location = std::stod(params.at("m"));
    double scale = std::stod(params.at("s"));
    return new TLogisticGenerator(location, scale);
  }
};



class TRandomFactory {
 private:
  std::map<std::string, TRandomCreator*> Creators; // three types of distributions
  TRandomFactory() {}
  ~TRandomFactory() {}
  TRandomFactory(const TRandomFactory &);
  TRandomFactory &operator = (const TRandomFactory &);
 public:
  static TRandomFactory &Instance() {
    static TRandomFactory Factory;
    static bool IsInitialized = false;
    static TNormalCreator NormalCreator;
    static TBernoulliCreator BernoulliCreator;
    static TLogisticCreator LogisticCreator;

    if (!IsInitialized) {
      IsInitialized = true;
      Factory.Creators["gauss"] = &NormalCreator;
      Factory.Creators["bernoulli"] = &BernoulliCreator;
      Factory.Creators["logistic"] = &LogisticCreator;
    }
    return Factory;

  }

  TRandomNumberGenerator *MakeGenerator(std::map<std::string, std::string> &params) {
    return Creators[params.at("type")]->CreateObject(params);
  }
};
